import 'package:flutter/material.dart';
import 'package:w8_519h0077_lt/next_page.dart';
import 'package:w8_519h0077_lt/sakai_services.dart';
import 'package:http/http.dart' as http;


class LoginPage extends StatefulWidget {
  const LoginPage({ Key? key }) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    _usernameController.dispose();
    _passwordController.dispose();
  }

  void login() async {
    var sakaiServices = SakaiService(sakaiUrl: 'https://xlms.myworkspace.vn');
    http.Response response = await sakaiServices.authenticate(_usernameController.text, _passwordController.text);

    if (response.statusCode == 200 || response.statusCode == 201){
      Navigator.of(context).push(MaterialPageRoute(builder: (context) => NextPage(username: _usernameController.text, password: _passwordController.text)));
    }
    else{
      
    }

  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Log in xLMS"),
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Center(
          child: Column(
            children: [
              TextFormField(
                controller: _usernameController,
                decoration: const InputDecoration(hintText: 'Username', ),
              ),
              TextFormField(
                controller: _passwordController,
                obscureText: true,
                decoration: const InputDecoration(hintText: 'Password', ),
              ),
              const SizedBox(
                height: 20,
                child: Text(''),
              ),
              TextButton(
                onPressed: login,
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.blue),
                  padding: MaterialStateProperty.all(const EdgeInsets.symmetric(horizontal: 80))
                ),
                child: const Text('Sign in', style: TextStyle(color: Colors.white),), 
              )
            ],
          ),
        ),
      ),
    );
  }
}