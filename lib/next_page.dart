import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:w8_519h0077_lt/sakai_services.dart';

class NextPage extends StatefulWidget {
  final String username;
  final String password;
  const NextPage({ Key? key, required this.username, required this.password }) : super(key: key);

  @override
  State<NextPage> createState() => _NextPageState();
}

class _NextPageState extends State<NextPage> {
  
  List courses = [];
  
  @override
  void initState() {
    super.initState();
    getCourses();
  }

  void getCourses() async{
    var sakaiServices = SakaiService(sakaiUrl: 'https://xlms.myworkspace.vn');
    http.Response response = await sakaiServices.authenticate(widget.username, widget.password);
    var result = await sakaiServices.getSites();
    var jsonSites = json.decode(result.body);
    var count = jsonSites['site_collection'].length;
    for (int i = 0; i < count; i++){
      courses.add(jsonSites['site_collection'][i]['entityTitle']);
    }
    setState(() {
      
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Courses attended"),
      ),
      body: courses.isEmpty ? const Center(child: CircularProgressIndicator(),) :
      ListView.builder(
        itemCount: courses.length,
        itemBuilder: (context, index){
          return ListTile(
            title: Text(courses[index]),
          );
        })
    );
  }
}